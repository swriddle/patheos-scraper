#!/bin/bash

source ~/scrapeos/scripts/properties.sh

curl -XPUT "http://dev.swriddle.com:9200/$ELASTICSEARCH_INDEX_NAME" -d "{
	\"mappings\": {
		\"$ELASTICSEARCH_TYPE_NAME\": {
			\"properties\": {
				\"title\": {
					\"type\": \"string\",
                    \"analyzer\": \"standard\"
				},
				\"categories\": {
					\"type\": \"string\",
                    \"analyzer\": \"standard\"
				},
				\"tags\": {
					\"type\": \"string\",
                    \"analyzer\": \"standard\"
				},
				\"date\": {
					\"type\": \"date\",
					\"format\": \"basic_date_time_no_millis\"
				},
				\"authorship\": {
					\"type\": \"string\",
                    \"analyzer\": \"standard\"
				},
				\"comment_page_url\": {
					\"type\": \"string\",
                    \"index\": \"not_analyzed\"
				},
				\"post_url\": {
					\"type\": \"string\",
                    \"index\": \"not_analyzed\"
				},
				\"html_file_path\": {
					\"type\": \"string\",
                    \"index\": \"not_analyzed\"
				},
				\"mobi_file_path\": {
					\"type\": \"string\",
                    \"index\": \"not_analyzed\"
				},
                \"contents\": {
                    \"type\": \"string\",
                    \"analyzer\": \"standard\"
                }
			}
		}
	}
}
"
