#!/usr/bin/python3

from elasticsearch import Elasticsearch

import argparse
import subprocess
import sys

import db
import es
import util
import workingdir


def main():

    print('Raw args:', sys.argv)
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-disableCache', help='TODO: Forces the cache to be off', action='store_true')
    parser.add_argument('-blog', help='Blog ID (like from URL)', action='store', default=None)
    parser.add_argument('-pages', help='Page descriptor with page numbers, commas, dashes for ranges.', action='store', default=None)
    parser.add_argument('-printIds', help='Before exiting, print the comma-separated list of post IDs that were examined on one line, and a comma-separated list of post IDs that were just downloaded on the following line.', action='store_true')
    parser.add_argument('-comments', help='Use this flag to backfill comments in addition to post data', action='store_true')
    args = parser.parse_args()

    # Initialize everything!

    if args.blog is None:
        print('Please specify a blog name')
        return 1
    if args.pages is None:
        print('Please specify a page expression')
        return 1

    if not workingdir.initialize_working_directory():
        print('Error: cannot initialize working directory')
        return 2
    if not db.initialize_database():
        print('Error: cannot initialize database')
        return 3


    if es.check_elasticsearch_running():
        print('Elasticsearch is running')
    else:
        print('Elasticsearch is NOT running')

    util.backfill(args)


if __name__ == '__main__':
    sys.exit(main())
