import json
import os
import sqlite3

import constants
import util

from PostMetadata import PostMetadata

configuration = None
with open(constants.get_configuration_file(), 'r') as f:
    configuration = json.load(f)
DB_FILENAME = configuration['DB_FILENAME']
TABLE_NAME = configuration['TABLE_NAME']
COMMENT_TABLE_NAME = configuration['COMMENT_TABLE_NAME']
WORKING_DIRECTORY = configuration['WORKING_DIRECTORY']

def get_all_id_tuples(conn=None, cursor=None):
    lazy_connection_tuple = lazy_connect(conn=conn, cursor=cursor)
    conn, cursor, _, _ = lazy_connection_tuple

    cursor.execute('select blog_id, id from {}'.format(TABLE_NAME))

    rows = cursor.fetchall()

    post_tuples = []
    for (blog_name, post_id) in rows:
        post_tuples.append((blog_name, post_id))

    lazy_close(lazy_connection_tuple)
    return post_tuples

def get_cache_filenames(blog_name, ids, conn=None, cursor=None):
    lazy_connection_tuple = lazy_connect(conn=conn, cursor=cursor)
    conn, cursor, _, _ = lazy_connection_tuple
    # stuff
    file_paths = []
    for id in ids:
        cursor.execute('select html_file_path from {} where blog_id = ? and id = ?'.format(TABLE_NAME), (blog_name, id))
        result = cursor.fetchone()
        file_path = result[0]
        file_paths.append(file_path)
    lazy_close(lazy_connection_tuple)
    return file_paths

def _get_db_filename():
    return os.path.join(WORKING_DIRECTORY, DB_FILENAME)

def initialize_database():
    filename = _get_db_filename()
    
    if not os.path.isfile(filename):
        _initialize_database(filename)
        return True
    else:
        # NOTE: Maybe insert a check for corrupt databases, if possible
        return True

def _initialize_database(filename):
    print('Initializing database...')
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('create table {}(blog_id text, id integer, title text, date text, authorship text, comment_page_url text, post_url text, html_file_path text, mobi_file_path text, categories text, tags text)'.format(TABLE_NAME))
    conn.commit()
    c.close()
    c = conn.cursor()
    c.execute('create table {}(post_url text, html_file_path text, mobi_file_path text)'.format(COMMENT_TABLE_NAME))
    conn.commit()
    c.close()
    conn.close()

def connect():
    filename = _get_db_filename()
    conn = sqlite3.connect(filename)
    return conn

#def 

def lazy_connect(conn=None, cursor=None):
    # Returns: (conn, cursor, conn_lazy_initialized, cursor_lazy_initialized)
    implicitly_opened_conn = True
    implicitly_opened_cursor = True
    if cursor is None:
        if conn is None:
            conn = connect()
            implicitly_opened_conn = True
        cursor = conn.cursor()
        implicitly_opened_cursor = True
    return conn, cursor, implicitly_opened_conn, implicitly_opened_cursor


def lazy_close(lazy_connection_tuple):
    conn, cursor, conn_was_lazy_initialized, cursor_was_lazy_initialized = lazy_connection_tuple
    if cursor_was_lazy_initialized:
        cursor.close()
    if conn_was_lazy_initialized:
        conn.close()

def create_insert_statement(table_name, m):
    d = util.generate_metadata_dictionary(m, flatten=True)
    ks = []
    vs = []
    for (k, v) in d.items():
        ks.append(k)
        vs.append(v)

    field_name_string = ', '.join(ks)
    question_mark_string = ', '.join(['?'] * len(d))
    
    statement = 'insert into {} ({}) values ({})'.format(table_name, field_name_string, question_mark_string)
    return statement, vs

def database_metadata_by_metadata(metadata, conn=None, cursor=None):
    blog_name = metadata.blog_id
    post_id = metadata.id
    return database_metadata_by_blog_id(blog_name, post_id, conn=conn, cursor=cursor)

def update_cache_files(m, conn=None, cursor=None):

    blog_name = m.blog_id
    post_id = m.id

    html_cache_file = m.local_html_file
    mobi_cache_file = m.local_mobi_file

    lazy_connection_tuple = lazy_connect(conn=conn, cursor=cursor)
    conn, cursor, _, _ = lazy_connection_tuple

    cursor.execute('update {} set html_file_path = ?, mobi_file_path = ? where blog_id = ? and id = ?'.format(TABLE_NAME), (html_cache_file, mobi_cache_file, blog_name, post_id))

    lazy_close(lazy_connection_tuple)


def comment_metadata_by_post_url(post_url, conn=None, cursor=None):
    lazy_connection_tuple = lazy_connect(conn=conn, cursor=cursor)
    conn, cursor, _, _ = lazy_connection_tuple
    query = 'select count(*) as count from {} where post_url = ?'.format(COMMENT_TABLE_NAME)
    cursor.execute(query, (post_url,))
    row = cursor.fetchone()
    if row[0] == 1:
        cursor.execute('select html_file_path, mobi_file_path from {} where post_url = ?'.format(COMMENT_TABLE_NAME), (post_url,))
        row = cursor.fetchone()
        html_file_path, mobi_file_path = row
        metadata = util.create_metadata({'post_url': post_url, 'comment_html_file': html_file_path, 'comment_mobi_file': mobi_file_path, 'origin': 'db.comment_metadata_by_post_url'})
        lazy_close(lazy_connection_tuple)
        return metadata
    elif row[0] == 0:
        lazy_close(lazy_connection_tuple)
        return None
    else:
        lazy_close(lazy_connection_tuple)
        raise Exception('This really shouldn\t happen')

    lazy_close(lazy_connection_tuple)

def database_metadata_by_blog_id(blog_name, blog_id, conn=None, cursor=None):
    lazy_connection_tuple = lazy_connect(conn=conn, cursor=cursor)
    conn, cursor, _, _ = lazy_connection_tuple
    cursor.execute('select count(*) as count from {} where blog_id = ? and id = ?'.format(TABLE_NAME), (blog_name, blog_id))
    row = cursor.fetchone()
    if row[0] == 1:
        cursor.execute('select blog_id, id, title, date, authorship, categories, tags, comment_page_url, post_url, html_file_path, mobi_file_path from {} where blog_id = ? and id = ?'.format(TABLE_NAME), (blog_name, blog_id))
        row = cursor.fetchone()
        blog_id, id, title, date, authorship, categories, tags, comment_page_url, post_url, html_file_path, mobi_file_path = row

        attributes = {}
        attributes['blog_id'] = blog_id
        attributes['id'] = id
        attributes['title'] = title
        attributes['time'] = date
        authors = authorship.split(' / ')
        attributes['authors'] = [x.strip() for x in authors]
        categories = categories.split(' / ')
        attributes['categories'] = [x.strip() for x in categories]
        tags = tags.split(' / ')
        attributes['tags'] = [x.strip() for x in tags]
        attributes['comment_page_url'] = comment_page_url
        attributes['post_url'] = post_url
        attributes['excerpt_length'] = -1
        attributes['local_html_file'] = html_file_path
        attributes['local_mobi_file'] = mobi_file_path
        attributes['comment_html_file'] = None
        attributes['comment_mobi_file'] = None
        attributes['origin'] = 'db.database_metadata_by_blog_id'

        metadata = util.create_metadata(attributes)

        lazy_close(lazy_connection_tuple)
        return metadata
    elif row[0] == 0:
        lazy_close(lazy_connection_tuple)
        return None
    else:
        lazy_close(lazy_connection_tuple)
        raise Exception('This really shouldn\t happen')


def insert_metadata(m, conn=None, cursor=None):
    lazy_connection_tuple = lazy_connect(conn=conn, cursor=cursor)
    conn, cursor, _, _ = lazy_connection_tuple
    insert_statement, insert_substitution_list = create_insert_statement(TABLE_NAME, m)
    cursor.execute(insert_statement, insert_substitution_list)
    conn.commit()
    lazy_close(lazy_connection_tuple)

def insert_comment_metadata(m, conn=None, cursor=None):
    lazy_connection_tuple = lazy_connect(conn=conn, cursor=cursor)
    conn, cursor, _, _ = lazy_connection_tuple
    insert_statement = 'insert into {}(post_url, html_file_path, mobi_file_path) values(?, ?, ?)'.format(COMMENT_TABLE_NAME)
    cursor.execute(insert_statement, (m.post_url, m.comment_html_file, m.comment_mobi_file))
    conn.commit()
    lazy_close(lazy_connection_tuple)
