import os

def get_configuration_file():
    default_path = '/home/sean/scrapeos/conf/configuration.json'
    using_default = True
    path = None

    #if 'SCRAPEOS_HOME' in os.environ:
    #    home_dir = os.environ['SCRAPEOS_HOME']
    #    conf_subpath = 'conf/configuration.json'
    #    path = os.path.join(home_dir, conf_subpath)
    #    using_default = False
    home_dir = '/home/sean/scrapeos'
    conf_subpath = 'conf/configuration.json'
    path = os.path.join(home_dir, conf_subpath)
    using_default = False


    if using_default:
        print('WARNING: Probably don\'t want to be using the default at this point')
        path = default_path

    return path
