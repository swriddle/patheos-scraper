#!/usr/bin/python3

import argparse
import os
import sys
import tempfile

import db
import util
import workingdir


def main():

    parser = argparse.ArgumentParser(description='Print all blog/ID pairs.')
    args = parser.parse_args()

    tuples = db.get_all_id_tuples()

    for blog, post_id in tuples:
        print('{},{}'.format(blog, post_id))



if __name__ == '__main__':
    sys.exit(main())
