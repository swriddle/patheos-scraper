Find post divs on a Patheos blog front page
-------------------------------------------

* div id=inner seems to hold the main content of the front page of a Patheos blog.
* Inside that, div id=content-sidebar-wrap seems to definitely exist. Could just enforce this relationship (rather than just unwrapping this div) as an extra check against a page format change.
* Inside this is id=content. Can enforce this relationship to existing as check against PFC.
* Under this will be a series of divs including class=post (or class=type-post, or maybe both) and a bunch of other IDs that correspond to the tags of the post.

Find metadata of a front page blog post, given post div
-------------------------------------------------------

* Directly inside of the top-level div you will find:
	* h2 class=entry-title will contain a single child anchor tag. The title attribute of this seems to be the best way to get the post title.
	* div class=post-info will contain:
		1) First, a child span class="date published time" will have a title attribute with a parseable timestamp.
		2) Second, a child span class="author vcard"
			- Which contains in this case a single child span class="fn", but in general there might exist one per author. Under this is a single anchor tag with link to author profile and author name in title attribute.
		3) Last, a child span class="post-comments". Child to this is a single anchor tag with link to the comment thread for this post, an attribute containing proprietary forum stuff, and containing in the text of the link itself a parseable number of comments.
	* div class=entry-content will contain:
		1) First, a link to the post itself.
		2) Second, a paragraph excerpt from the post, ending with an inline link to the post.
		3) Last, a paragraph of links to social networking stuff.
	* div class=post-meta will contain:
		1) span class=categories. Metadata about associated categories, which can be pulled from text of child link.
		2) span class=tags. Metadata about associated tags, which can be pulled from text of child link.
		3) span class=post-comments. Metadata about number of comments, repeated from above.

Find post content of a blog post, given post URL
-------------------------------------------------------

* Generate soup from URL
* Confirm the direct parent-child relation of div id=wrap, div id=inner, div id=content-sidebar-wrap, div id=content, div class has "post type-post", div class=entry-content.
* The contents of div class=entry-content can be put directly into an HTML page.
