#!/usr/bin/python3

import argparse
import sys

import util


def main():

    parser = argparse.ArgumentParser(description='Display all commands')
    args = parser.parse_args()

    commands = util.available_subcommands

    print(' '.join(commands))

    return 0

if __name__ == '__main__':
    sys.exit(main())
