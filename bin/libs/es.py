from elasticsearch import Elasticsearch

import json
import re
import urllib.request

import constants
import util

es_cursor = Elasticsearch(['http://dev.swriddle.com:9200'])

configuration = None
with open(constants.get_configuration_file(), 'r') as f:
    configuration = json.load(f)
ELASTICSEARCH_HOST = configuration['ELASTICSEARCH_HOST']
ELASTICSEARCH_PORT = configuration['ELASTICSEARCH_PORT']
ELASTICSEARCH_INDEX_NAME = configuration['ELASTICSEARCH_INDEX_NAME']
ELASTICSEARCH_TYPE_NAME = configuration['ELASTICSEARCH_TYPE_NAME']

def check_elasticsearch_running():
    es_status = es_cursor.info()
    return 'status' in es_status.keys() and es_status['status'] == 200

def index_document(doc, id=None, type=None):
    if type is None:
        type = ELASTICSEARCH_TYPE_NAME
    if id is None:
        res = es_cursor.index(index=ELASTICSEARCH_INDEX_NAME, doc_type=type, body=doc)
    else:
        res = es_cursor.index(index=ELASTICSEARCH_INDEX_NAME, doc_type=type, body=doc, id=id)

#
#    'title', 'date', 'tags', 'categories', 'authorship', 'comment_page_url', 'post_url', 'file_path', 'contents'
#def update_cache(blog_name, post_id, new_filename):
#    es_cursor.update(ELASTICSEARCH_INDEX_NAME, blog_name, post_id, 
#def delete_from_cache(metadata):
#    es_cursor.delete(ELASTICSEARCH_INDEX_NAME, metadata.blog_id, metadata.id)
#
def index_metadata(m, update=False):
    d = util.generate_metadata_dictionary(m, flatten=False)
    d['date'] = util.convert_to_elasticsearch_date(d['date'])
    soup = m.get_post_soup()
    texts = soup.findAll(text=True)
    visible_texts = filter(visible, texts)
    all_the_texts = '\n'.join(visible_texts)
    d['content'] = all_the_texts

    # Rewrite 'blog_id' as the type
    blog_id = d['blog_id']
    del d['blog_id']

    # Rewrite 'id' as the internal id
    post_id = d['id']
    del d['id']
    if update:
        update_document(d, type=blog_id, id=post_id)
    else:
        index_document(d, type=blog_id, id=post_id)

def update_document(d, type=None, id=None):
    assert type is not None and id is not None
    update_message = json.dumps({'doc': d})
    res = es_cursor.update(index=ELASTICSEARCH_INDEX_NAME, doc_type=type, id=id, body=update_message)
    print('Update result: {} ({})'.format(res, res.__class__))

def update_cache_files(m):
    index_metadata(m, update=True)

def visible(element):
    if element.parent.name in ('style', 'script', '[document]', 'head', 'title'):
        return False
    elif re.match('<!--.*-->', str(element)):
        return False
    else:
        return True

def delete_elasticsearch_index():
    if es_cursor.indices.exists(ELASTICSEARCH_INDEX_NAME):
        print('WARNING: Dropping Elasticsearch index "{}"'.format(ELASTICSEARCH_INDEX_NAME))
        es_cursor.indices.delete(ELASTICSEARCH_INDEX_NAME)

def define_elasticsearch_mapping_for_blog(blog_id):

    non_analyzed_string = {'type': 'string', 'index': 'not_analyzed'}
    standard_string = {'type': 'string', 'analyzer': 'standard'}
    date_type = {'type': 'date', 'format': 'basic_date_time_no_millis'}
    properties = {'title': standard_string, 'date': date_type, 'tags': standard_string, 'categories': standard_string, 'authorship': standard_string, 'comment_page_url': non_analyzed_string, 'post_url': non_analyzed_string, 'html_file_path': non_analyzed_string, 'mobi_file_path': non_analyzed_string, 'contents': standard_string}
    mappings_section = {blog_id: {'properties': properties}}
    es_cursor.indices.put_mapping(blog_id, mappings_section)

def has_mapping(mapping_name):
    if es_cursor.indices.exists(ELASTICSEARCH_INDEX_NAME):
        mapping_data = es_cursor.indices.get_mapping(ELASTICSEARCH_INDEX_NAME, mapping_name)
        return len(mapping_data) > 0
