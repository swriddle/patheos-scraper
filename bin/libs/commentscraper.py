import csv
import http.cookiejar
import itertools
import json
import os
import random
import sys
import tempfile
import time
import urllib.request

import workingdir



cj = http.cookiejar.CookieJar()
opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))

def main(args):
    url = 'http://www.patheos.com/blogs/slacktivist/2015/07/16/when-did-adam-and-eve-get-married/'
    scrape_comments(url)

def create_comment_html_from_csv_filename(filename):
    post_htmls = []
    with open(filename, 'r', newline='') as f:

        csv_reader = csv.reader(f, delimiter='\t')
        field_names = csv_reader.__next__()
        field_names_to_index = {field_name: i for (i, field_name) in enumerate(field_names)}

        content_index = field_names_to_index['post_message']
        author_name_index = field_names_to_index['post_author_name']
        author_username_index = field_names_to_index['post_author_username']
        post_message_index = field_names_to_index['post_message']
        plain_message_index = field_names_to_index['post_raw_message']
        post_id_index = field_names_to_index['post_id']
        post_parent_index = field_names_to_index['post_parent']
        author_display_template = '{name} ({username})'
        message_content = {}
        for row in reversed(list(csv_reader)):
            post_content = row[post_message_index]
            post_author_name = row[author_name_index]
            post_author_username = row[author_username_index]
            plain_message = row[plain_message_index]
            post_id = row[post_id_index]
            post_parent = row[post_parent_index]
            message_content[post_id] = plain_message
            comment_excerpt = None

            #examined = post_parent == '2170356496'
            examined = False

            if post_parent != '':
                if post_parent in message_content:
                    #comment_excerpt = message_content[post_parent][:100]
                    excerpt_from = message_content[post_parent]
                    lowered = excerpt_from.lower()
                    excerpt_length_near = 200
                    link_start = lowered.rfind('<a ', 0, excerpt_length_near)
                    link_end = lowered.rfind('</a>', 0, excerpt_length_near)
                    link_end += len('</a>')
                    are_in_link = link_start != -1 and link_end < link_start

                    if are_in_link:
                        link_end = lowered.rfind('</a>', excerpt_length_near)
                        link_end += len('</a>')
                        comment_excerpt = excerpt_from[:link_end]
                    else:
                        comment_excerpt = excerpt_from[:excerpt_length_near]

                    if examined:
                        print('--------examined--------')
                        print('Response length:', len(lowered))
                        print('Link start:', link_start)
                        print('Link end:', link_end)
                        if are_in_link:
                            print('In link')
                        else:
                            print('not in link')
                        print('Original response: "{}"'.format(excerpt_from))
                        if are_in_link:
                            print('excerpt with link handling: "{}"'.format(comment_excerpt))
                        else:
                            print('excerpt - NO link handling: "{}"'.format(comment_excerpt))




                    #if 'never existed' in comment_excerpt:
                    #    print('Okay...I found it!')
                    #    print('Comment excerpt: "{}"'.format(comment_excerpt))

                else:
                    comment_except = 'original message not found'
                
            #comment_excerpt = None if post_parent == '' else message_content[post_parent][:100]


            author_display_string = author_display_template.format(name=post_author_name, username=post_author_username)

            post_html = None
            if comment_excerpt is None:
                post_html = '<p>{author_string}</p>\n{post}\n'.format(author_string=author_display_string, post=post_content)
            else:
                post_html = '<p>{author_string}</p>\n<p><h6>Responding to: {references_excerpt}</h6></p>\n{post}\n'.format(references_excerpt=comment_excerpt, author_string=author_display_string, post=post_content)

            post_htmls.append(post_html)
    comment_body_html = '\n<hr />\n'.join(post_htmls)
    body = '<body>\n{}\n</body>'.format(comment_body_html)
    html_page = '<html>\n<head>\n<title>{title}</title>\n</head>\n{body}\n</html>\n'.format(title='blah', body=body)

    top_working_directory = workingdir.get_working_directory()
    working_directory = os.path.join(top_working_directory, 'cache')

    fd, filename = tempfile.mkstemp(prefix='comments-', suffix='.html', dir=working_directory)
    with os.fdopen(fd, 'w') as f:
        f.write(html_page)
    print('Wrote comment HTML:', filename)
    return filename

def scrape_comments(input_url):
    print('scrape start')

    url = 'https://tools.digitalmethods.net/beta/disqusScraper/?json=createjob'
    url_request = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'})
    print('Creating job')
    response_data = opener.open(url_request).read().decode('utf8', 'ignore')
    print('Response data:', response_data)
    response_dict = json.loads(response_data)
    job_id = response_dict['jobid']
    print('Created job:', job_id)

    request_dict = {}

    request_dict['of'] = 'lv'
    request_dict['urls'] = input_url
    request_data = urllib.parse.urlencode(request_dict).encode('utf8')

    print('Going to send syn')
    url = 'https://tools.digitalmethods.net/beta/disqusScraper/?jobid={job_id}&json=syn'.format(job_id=job_id)
    url_request = urllib.request.Request(url, request_data, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'})
    response_data = opener.open(url_request).read().decode('utf8', 'ignore')
    print('Syn sent')

    jigger_range = 500
    base_values = (520, 1200, 732, 830, 1739, 2523) # Just some numbers. No magic here.
    stop_at_threshold = 20
    i = 0
    for base_value in itertools.cycle(base_values):
        if i == stop_at_threshold:
            print('Breaking out!')
            break
        real_value = base_value + random.randint(0, jigger_range)
        print('Waiting...')
        time.sleep(real_value / 1000)
        print('Back!')
        messages, is_finished = check(job_id)
        if is_finished:
            print('Good, actually finished.')
            csv_filename = retrieve_results(job_id)
            print('CSV: {}'.format(csv_filename))
            return csv_filename
        i += 1
    

def retrieve_results(job_id):
    url = 'https://tools.digitalmethods.net/beta/disqusScraper/?jobid={job_id}&json=result&view=renderCSVResults'.format(job_id=job_id)
    url_request = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'})
    response_data = opener.open(url_request).read().decode('utf8', 'ignore')
    fd, filename = tempfile.mkstemp(prefix='results-', suffix='.csv')
    with os.fdopen(fd, 'w') as f:
        f.write(response_data)
    print('CSV written:', filename)
    return filename

def check(job_id):
    url = 'https://tools.digitalmethods.net/phplib/jsonpipe.php?jobid={job_id}&_={time}'.format(job_id=job_id, time=time.gmtime())
    print('Check URL:', url)
    url_request = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'})
    response_data = opener.open(url_request).read().decode('utf8', 'ignore')
    print('Check data received')
    events = json.loads(response_data)
    processors = {'JSONlogEntryString': parse_json_log}
    messages = []
    is_finally_finished = False
    for event in events:
        event_type = event['event']
        processor = processors[event_type]
        payload, is_finished = processor(event)
        messages.append(payload)
        print('Payload:', payload)
        if is_finished:
            print('All done!')
            is_finally_finished = True
            break
    return messages, is_finally_finished

def parse_json_log(event):
    event_payload = event['data']['msg']
    return event_payload, 'Job is finished' in event_payload

if __name__ == '__main__':
    sys.exit(main(sys.argv))
