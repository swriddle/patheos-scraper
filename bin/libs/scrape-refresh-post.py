#!/usr/bin/python3

import argparse
import os
import sys
import tempfile

import db
import util
import workingdir


def main():

    parser = argparse.ArgumentParser(description='Re-downloads the URL associated with the post.')
    parser.add_argument('-blog', help='Blog ID (like from URL)', action='store', default=None, required=True)
    parser.add_argument('-ids', help='Comma separated list of post IDs', action='store', default=None, required=True)
    parser.add_argument('-title', help='Title for the book', action='store', default='(untitled)')
    args = parser.parse_args()

    print_ids = args.printIds

    ids = args.ids
    ids = ids.split(',')
    ids = [int(x.strip()) for x in ids]

    downloaded_ids = []

    metadatas = []
    for id in ids:
        metadata = db.database_metadata_by_blog_id(args.blog, id)
        metadatas.append(metadata)

    for metadata in metadatas:
        metadata.clear_download_mark()
        metadata.regenerate_from_post_url()
        if metadata.get_download_mark():
            downloaded_ids.append(metadata)
        print('Updating metadata...')
        util.update_cache_files(metadata)
        print('Done updating metadata.')


    if print_ids:
        print(','.join([str(x) for x in ids]))
        print(','.join([str(x) for x in downloaded_ids]))


if __name__ == '__main__':
    sys.exit(main())
