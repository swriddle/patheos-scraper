import bs4
import os
import re
import subprocess
import tempfile
import urllib

import commentscraper
import db
import es
import workingdir

import util

class PostMetadata(object):
    def __init__(self):
        self.blog_id = None
        self.id = None
        self.title = None
        self.time = None
        self.authors = None
        self.comment_page_url = None
        self.post_url = None
        self.excerpt_length = None
        self.local_html_file = None
        self.local_mobi_file = None
        self.categories = None
        self.tags = None
        self.was_downloaded = False
        self.comment_html_file = None
        self.comment_mobi_file = None
        self.origin = None

    def __str__(self):
        return '...'

    def download_comments_html(self):
        if self.comment_html_file is None:
            csv_filename = commentscraper.scrape_comments(self.post_url)
            html_filename = commentscraper.create_comment_html_from_csv_filename(csv_filename)
            self.comment_html_file = html_filename
        return self.comment_html_file

    def delete_from_cache(self):
        if self.local_html_file is not None:
            os.unlink(self.local_html_file)
            self.local_html_file = None
        if self.local_mobi_file is not None:
            os.unlink(self.local_mobi_file)
            self.local_mobi_file = None
        db.delete_from_cache(self)
        es.delete_from_cache(self)

    def generate_comment_mobi(self):
        if self.comment_mobi_file is None:
            local_file = self.download_comments_html()
            top_working_directory = workingdir.get_working_directory()
            working_directory = os.path.join(top_working_directory, 'cache')
            self.comment_mobi_file = util.generate_temp_mobi(local_file, dir=working_directory, prefix='generatedComments-', suffix='.mobi', authors=('Various',), title='Comments ({})'.format(self.title))
        return self.comment_mobi_file

    def generate_mobi(self):
        if self.local_mobi_file is None:
            local_file = self.download_post()
            top_working_directory = workingdir.get_working_directory()
            working_directory = os.path.join(top_working_directory, 'cache')
            self.local_mobi_file = util.generate_temp_mobi(local_file, dir=working_directory, prefix='generated-', suffix='.mobi', authors=self.authors, title=self.title)
        return self.local_mobi_file




    def download_post(self):
        if self.local_html_file is not None:
            return self.local_html_file
        top_working_directory = workingdir.get_working_directory()
        working_directory = os.path.join(top_working_directory, 'cache')
        print('Downloading page...', end='')
        self.mark_downloaded()
        page = urllib.request.urlopen(self.post_url)
        page_data = page.read()
        page.close()
        print('!')
        soup = bs4.BeautifulSoup(page_data)
        post_element = soup.find(attrs={'class':'post'})
        fd, self.local_html_file = tempfile.mkstemp(suffix='.html', prefix='post-', dir=working_directory)
        with os.fdopen(fd, 'w') as f:
            f.write('<html>\n')
            f.write('<head>\n')
            f.write('<title>{}</title>\n'.format(self.title))
            f.write('</head>\n')
            f.write('<body>\n')
            f.write(str(post_element))
            f.write('</body>\n')
            f.write('</html>\n')
        return self.local_html_file

    def clear_download_mark(self):
        self.was_downloaded = False

    def get_download_mark(self):
        return self.was_downloaded

    def mark_downloaded(self):
        self.was_downloaded = True

    def get_post_soup(self):
        filename = self.download_post()
        page_data = None
        with open(filename, 'r') as f:
            page_data = f.read()
        soup = bs4.BeautifulSoup(page_data)
        return soup

    def index(self):
        self.download_post()
        es.index_metadata(self)

    def regenerate_from_local_file(self):
        soup = None
        with open(self.local_html_file, 'r') as f:
            soup = bs4.BeautifulSoup(f)

        post_tag = util._select(soup, 'div#post')
        
        if post_tag is None:
            print('WARNING1: Could not find post tag on post URL.')
            return


        m = util.get_post_metadata_from_front_page(post_tag, self.blog_id)

        
        # copy it over
        self.blog_id = m.blog_id
        self.id = m.id
        self.title = m.title
        self.time = m.time
        self.authors = m.authors
        self.comment_page_url = m.comment_page_url
        self.post_url = m.post_url
        self.excerpt_length = m.excerpt_length
        self.local_html_file = m.local_html_file
        self.local_mobi_file = m.local_mobi_file
        self.categories = m.categories
        self.tags = m.tags
        self.comment_html_file = m.comment_html_file
        self.comment_mobi_file = m.comment_mobi_file





    def regenerate_from_post_url(self):
        
        blog_name_re = re.compile(r'http://www\.patheos\.com/blogs/([^/]+)/.*')
        blog_name_matcher = blog_name_re.match(self.post_url)
        blog_name = blog_name_matcher.groups()[0]

        page = urllib.request.urlopen(self.post_url)
        page_data = page.read()
        page.close()

        fd, filename = tempfile.mkstemp(suffix='.html', prefix='page-')
        with os.fdopen(fd, 'w') as f:
            f.write(str(page_data))
        print('Page written to:', filename)

        soup = bs4.BeautifulSoup(page_data)

        post_tag = soup.find('div', class_=lambda x: x == 'post')
        
        if post_tag is None:
            print('WARNING2: Could not find post tag on post URL.')
            return


        m = util.get_post_metadata_from_front_page(post_tag, blog_name)

        
        # copy it over
        self.blog_id = m.blog_id
        self.id = m.id
        self.title = m.title
        self.time = m.time
        self.authors = m.authors
        self.comment_page_url = m.comment_page_url
        self.post_url = m.post_url
        self.excerpt_length = m.excerpt_length
        self.local_html_file = m.local_html_file
        self.local_mobi_file = m.local_mobi_file
        self.categories = m.categories
        self.tags = m.tags
        self.comment_html_file = m.comment_html_file
        self.comment_mobi_file = m.comment_mobi_file

        return


    def __str__(self):
        return '...'

    def comment_db_insert(self):
        self.download_comments_html()
        self.generate_comment_mobi()
        db.insert_comment_metadata(self)
        pass

    def db_insert(self):
        self.download_post()
        db.insert_metadata(self)
