#!/bin/bash

ELASTICSEARCH_INDEX_NAME_RAW=`cat ~/scrapeos/conf/configuration.json | jshon -e ELASTICSEARCH_INDEX_NAME`
ELASTICSEARCH_TYPE_NAME_RAW=`cat ~/scrapeos/conf/configuration.json | jshon -e ELASTICSEARCH_TYPE_NAME`

remove_quotes_regex="\"(.*)\""

ELASTICSEARCH_INDEX_NAME=""
if [[ $ELASTICSEARCH_INDEX_NAME_RAW =~ $remove_quotes_regex ]]; then
	ELASTICSEARCH_INDEX_NAME="${BASH_REMATCH[1]}"
fi

ELASTICSEARCH_TYPE_NAME=""
if [[ $ELASTICSEARCH_TYPE_NAME_RAW =~ $remove_quotes_regex ]]; then
	ELASTICSEARCH_TYPE_NAME="${BASH_REMATCH[1]}"
fi


