import bs4
import os
import re
import signal
import subprocess
import sys
import tempfile
import urllib.request

import commentscraper
import db
import es
import workingdir
import PostMetadata

available_subcommands = ('reset', 'init', 'backfill', 'book', 'ls', 'refresh-post', 'raw', 'print-ids')

def create_metadata(metadata_items):
    metadata = PostMetadata.PostMetadata()
    print('origin: {}, keys: {}'.format(metadata_items['origin'], sorted(metadata_items.keys())))
    for (k, v) in metadata_items.items():
        setattr(metadata, k, v)
    return metadata

'''
        self.blog_id = None
        self.id = None
        self.title = None
        self.time = None
        self.authors = None
        self.comment_page_url = None
        self.post_url = None
        self.excerpt_length = None
        self.local_html_file = None
        self.local_mobi_file = None
        self.categories = None
        self.tags = None
        self.was_downloaded = False
        self.comment_html_file = None
        self.comment_mobi_file = None
        self.origin = None
'''

def backfill(args):

    blog_name = args.blog
    pages = get_page_list(args.pages)
    disable_cache = args.disableCache
    print_ids = args.printIds
    download_comments = args.comments

    # Have to do this
    if not es.has_mapping(blog_name):
        es.define_elasticsearch_mapping_for_blog(blog_name)
        print('Type declared.')
    else:
        print('Looks like we don\'t need to declare a type this time')

    downloaded_ids = []

    metadatas = []
    for page in pages:
        soup = get_soup_for_blog(blog_name, page)
        post_divs = get_post_divs_from_front_page(soup)
        for post_div in post_divs:
            metadata = get_post_metadata_from_front_page(post_div, blog_name)
            #metadata.clear_download_mark()
            metadatas.append(metadata)

    ids = [metadatum.id for metadatum in metadatas]

    for metadata in metadatas:
        if disable_cache:
            metadata.generate_mobi()
            metadata.index()
            metadata.db_insert()
            if metadata.get_download_mark():
                downloaded_ids.append(metadata.id)
        else:
            db_metadata= db.database_metadata_by_metadata(metadata)
            if db_metadata is None:
                metadata.generate_mobi()
                metadata.index()
                metadata.db_insert()

        if download_comments:

            if disable_cache:
                metadata.download_comments_html()
                metadata.generate_comment_mobi()
                metadata.comment_db_insert()
                # TODO: could index comments in a new ES index, but not yet.
            else:
                db_metadata = db.comment_metadata_by_post_url(metadata.post_url)
                if db_metadata is None:
                    metadata.download_comments_html()
                    metadata.generate_comment_mobi()
                    metadata.comment_db_insert()


    if print_ids:
        print(','.join([str(x) for x in ids]))
        print(','.join([str(x) for x in downloaded_ids]))

def get_full_cached_metadata_by_blog_id(blog_name, post_id):
    db_metadata = db.database_metadata_by_blog_id(blog_name, post_id)
    comment_metadata = db.comment_metadata_by_post_url(db_metadata.post_url)
    db_metadata.comment_html_file = comment_metadata.comment_html_file
    db_metadata.comment_mobi_file = comment_metadata.comment_mobi_file
    return db_metadata

RE_POST_ID = re.compile(r'post-([0-9]+)')

def generate_temp_mobi(filename, dir=None, suffix=None, prefix=None, authors=None, title=None):
    author_string = '&'.join(authors) if authors is not None else 'Anonymous'
    title = title if title is not None else 'Untitled'

    top_working_directory = workingdir.get_working_directory()
    working_directory = os.path.join(top_working_directory, 'cache')
    fd, output_filename = None, None
    if dir is None:
        fd, output_filename = tempfile.mkstemp(prefix=prefix, suffix=suffix)
    else:
        fd, output_filename = tempfile.mkstemp(prefix=prefix, suffix=suffix, dir=dir)
    print('call 1')
    command = ['ebook-convert', filename, output_filename, '--authors', author_string, '--title', title]
    subprocess.call(command)
    return output_filename

def get_soup_for_blog(blog_name, page):
    assert page is not None
    assert page >= 1
    
    url = 'http://www.patheos.com/blogs/slacktivist/'
    if page != 1:
        url = 'http://www.patheos.com/blogs/slacktivist/page/{}/'.format(page)

    page = urllib.request.urlopen(url)
    page_data = page.read()
    page.close()

    soup = bs4.BeautifulSoup(page_data)

    return soup


def write_toc(f, metadatas, title=None):
    title = '(untitled)' if title is None else title
    f.write('<html>\n')
    f.write('<head>\n')
    f.write('<title>{}</title>\n'.format(title))
    f.write('</head>\n')
    f.write('<body>\n')
    f.write('<ol>\n')
    for metadata in metadatas:
        filename = metadata.download_post()
        f.write('<li><a href="file://{}">{}</a></li>\n'.format(filename, metadata.title))
    f.write('</ol>\n')
    f.write('</body>\n')
    f.write('</html>\n')

def _select(element, selector, just_pick_one=False):
    objs = element.select(selector)
    if len(objs) == 0:
        print('not found')
        return None
    if len(objs) != 1 and not just_pick_one:
        raise Exception('Expected a single element, not {}'.format(len(objs)))
    return objs[0]

def get_post_divs_from_front_page(soup):
    post_divs = soup.select('div#inner > div#content-sidebar-wrap > div#content > div.post')
    print('Found {} post divs'.format(len(post_divs)))
    return post_divs

# groups: years, months, days, hours, minutes, seconds
re_patheos_time = re.compile(r'([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})\+00:00')
iso_8601_format = '{}-{}-{} {}:{}:{}.000'
re_iso8601_time = re.compile(r'([0-9]{4})')
es_time_format = '{}{}{}T{}{}{}Z'

def convert_to_iso8601(date_string):
    # input example: 2015-07-08T14:18:31+00:00
    # output example: "YYYY-MM-DD HH:MM:SS.SSS"
    m = re.patheos_time.match(date_string)
    if m:
        date_tuple = m.groups()
        return iso_8601_format.format(*date_tuple)
    else:
        return None

def convert_to_elasticsearch_date(date_string):
    m = re_patheos_time.match(date_string)
    if m:
        date_tuple = m.groups()
        return es_time_format.format(*date_tuple)
    else:
        return None

def generate_metadata_dictionary(m, flatten=False):
    d = {}
    d['id'] = m.id
    d['blog_id'] = m.blog_id
    d['title'] = m.title
    d['date'] = m.time
    if flatten:
        d['authorship'] = ' / '.join(m.authors)
        d['categories'] = ' / '.join(m.categories)
        d['tags'] = ' / '.join(m.tags)
    else:
        d['authorship'] = m.authors
        d['categories'] = m.categories
        d['tags'] = m.tags
    d['comment_page_url'] = m.comment_page_url
    d['post_url'] = m.post_url
    d['html_file_path'] = m.local_html_file
    d['mobi_file_path'] = m.local_mobi_file


    return d
    

def merge_mobi(filenames, title=None, author=None, work_directory=None, output_directory=None):

    if title is None:
        title = '(untitled)'
    if author is None:
        author = 'Alan Smithee'
    if work_directory is None or output_directory is None:
        print('Please set both output and work directories for calls to mobi_merge')
        sys.exit(1)

    epub_filenames = []

    for filename in filenames:
        fd, epub_filename = tempfile.mkstemp(prefix='converted-', suffix='.epub', dir=work_directory)
        print('call 2')
        subprocess.call(['ebook-convert', filename, epub_filename])
        epub_filenames.append(epub_filename)

    fd, merge_file = tempfile.mkstemp(prefix='merged-', suffix='.epub', dir=work_directory)
    args = ['calibre-debug', '--run-plugin', 'EpubMerge', '--', '-t', title, '-a', author, '-o', merge_file]
    args.extend(epub_filenames)
    subprocess.call(args)
    print('Merge complete')
    fd, book_file = tempfile.mkstemp(suffix='.mobi', prefix='book-', dir=output_directory)

    print('call 3')
    subprocess.call(['ebook-convert', merge_file, book_file])

    print('Final file produced:', book_file)

    return book_file

def merge_mobi_metadata(metadatas, title=None, author=None, work_directory=None, output_directory=None, add_comments=None):

    if add_comments is None:
        add_comments = False

    if add_comments:
        print('Planning to add comments')
    else:
        print('Planning NOT to add comments')

    ebook_filenames = []
    for metadata in metadatas:
        ebook_filenames.append(metadata.local_mobi_file)
        if add_comments:
            ebook_filenames.append(metadata.comment_mobi_file)
    print('Going to make an ebook from {} books'.format(len(ebook_filenames)))
    return merge_mobi(ebook_filenames, title=title, author=author, work_directory=work_directory, output_directory=output_directory)

def init(args):
    executable_name = '/home/sean/scrapeos/scripts/elasticsearch/create_index.sh'
    subprocess.call([executable_name])
    workingdir.initialize_working_directory()
    db.initialize_database()

def reset(args):
    workingdir.delete()
    es.delete_elasticsearch_index()

def update_cache_files(m):
    es.update_cache_files(m)
    db.update_cache_files(m)

def get_page_list(page_string):
    # the string should only consist of [0-9,-]
    pages = set()
    parts = page_string.split(',')
    for part in parts:
        if '-' in part:
            start_of_range, end_of_range = part.split('-')
            pages.update(range(int(start_of_range), int(end_of_range) + 1))
        else:
            pages.add(int(part))
    t = tuple(pages)
    t = sorted(tuple(pages))
    return t


def get_post_metadata_from_front_page(post_div, blog_id, force_load=False):
    classes = post_div['class']


    metadata_map = {}
    post_classes = [x for x in classes if x.startswith('post-')]
    assert len(post_classes) == 1
    post_class = post_classes[0]

    matcher = RE_POST_ID.match(post_class)

    if matcher:
        metadata_map['id'] = int(matcher.groups()[0])
    else:
        metadata_map['id'] = None


    if not force_load:
        # attempt to load from cache
        db_metadata= db.database_metadata_by_blog_id(blog_id, metadata_map['id'])
        if db_metadata is not None:
            return db_metadata
        else:
            pass # Not found in cache

    metadata_map['blog_id'] = blog_id
    post_title_tag = _select(post_div, 'h2.entry-title')
    title_link = _select(post_title_tag, 'a')
    post_title = title_link['title']
    metadata_map['title'] = post_title
    post_info = _select(post_div, 'div.post-info')

    # Get post time
    post_time = _select(post_info, 'span.date.published.time')['title']
    # keep post_time
    metadata_map['time'] = post_time

    # Get post authorship
    post_author_info = _select(post_info, 'span.author.vcard')
    spans = post_author_info.select('span')
    authors = []
    for span in spans:
        authorship_link = _select(span, 'a')
        author_name = authorship_link['title']
        authors.append(author_name)
    metadata_map['authors'] = tuple(authors)

    # Get comment metadata
    comment_metadata = _select(post_info, 'span.post-comments')
    comment_link = _select(comment_metadata, 'a')
    comment_page_url = comment_link['href']
    # keep parseable_comment_count and comment_page_url
    metadata_map['comment_page_url'] = comment_page_url

    entry_content = _select(post_div, 'div.entry-content')
    excerpt = entry_content.find('p', attrs={'class': None})
    post_link = _select(excerpt, 'a')
    
    post_url = '*ignore*'
    if post_link is not None:
        post_url = post_link['href']
    # keep post_url and excerpt
    metadata_map['post_url'] = post_url
    metadata_map['excerpt_length'] = len(str(excerpt))

    post_meta = _select(post_div, 'div.post-meta')
    categories = _select(post_meta, 'span.categories')
    tags = _select(post_meta, 'span.tags')
    post_comments = _select(post_meta, 'span.post-comments')
    categories_text = _select(categories, 'a', just_pick_one=True).text

    category_names = []
    if categories is not None:
        category_elements = categories.select('a')
        for element in category_elements:
            category_text = element.text
            category_names.append(category_text)
    metadata_map['categories'] = tuple(category_names)

    tag_names = []
    if tags is not None:
        tag_elements = tags.select('a')
        for element in tag_elements:
            tag_text = element.text
            tag_names.append(tag_text)
    metadata_map['tags'] = tuple(tag_names)

    metadata_map['origin'] = 'util.get_post_metadata_from_front_page'

    metadata = create_metadata(metadata_map)

    return metadata


