#!/usr/bin/python3

import argparse
import bs4
import os
import sys
import tempfile
import urllib.request

import commentscraper
import db
import util
import workingdir


def main():

    parser = argparse.ArgumentParser(description='Re-downloads the URL associated with the post.')
    parser.add_argument('-blog', help='Blog ID (like from URL)', action='store', default=None, required=True)
    parser.add_argument('-ids', help='Comma separated list of post IDs', action='store', default=None, required=True)
    args = parser.parse_args()

    ids = args.ids
    ids = ids.split(',')
    ids = [int(x.strip()) for x in ids]

    metadatas = []
    for id in ids:
        metadata = db.database_metadata_by_blog_id(args.blog, id)
        metadatas.append(metadata)

    for metadata in metadatas:
        post_url = metadata.post_url
        comment_page_url = metadata.comment_page_url
        print('Post URL:', post_url)
        print('Comment URL:', comment_page_url)

        f = urllib.request.urlopen(post_url)
        content = f.read()
        f.close()

        fd, filename = tempfile.mkstemp(suffix='.html', prefix='page-')
        with os.fdopen(fd, 'w') as f:
            f.write(str(content))
        print('{}.{} -> {}'.format(metadata.blog_id, metadata.id, filename))

        print('Attempting comment scrape...')
        csv_filename = commentscraper.scrape_comments(post_url)
        html_filename = commentscraper.create_comment_html_from_csv_filename(csv_filename)
        print('Comment scrape complete...\nCSV: {}\nHTML: {}'.format(csv_filename, html_filename))



if __name__ == '__main__':
    sys.exit(main())
