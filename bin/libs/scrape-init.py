#!/usr/bin/python3

import sys

import argparse
import util


def main():

    parser = argparse.ArgumentParser(description='Initialize the Scrapeos software.')
    args = parser.parse_args()
    util.init(args)

if __name__ == '__main__':
    sys.exit(main())
