#!/usr/bin/python3

import argparse
import sys

import util


def main():

    parser = argparse.ArgumentParser(description='Reset the Scrapeos software.')
    args = parser.parse_args()

    util.reset(args)
    return 0

if __name__ == '__main__':
    sys.exit(main())
