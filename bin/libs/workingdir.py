import constants
import json
import os
import shutil

working_directory = None

def get_working_directory():
    global working_directory
    if working_directory is None:
        configuration = None
        with open(constants.get_configuration_file(), 'r') as f:
            configuration = json.load(f)
        if configuration is not None:
            working_directory = configuration['WORKING_DIRECTORY']
    return working_directory

def initialize_working_directory():
    working_directory = get_working_directory()
    if not os.path.exists(working_directory):
            _initialize_working_directory()
            return True # Everything is okay
    if os.path.isdir(working_directory):
            return True # Still okay
    else:
            return False # A non-directory file is in the way

def _initialize_working_directory():
    working_directory = get_working_directory()
    print('Initializing working directory...')
    os.mkdir(working_directory)
    cache_directory = os.path.join(working_directory, 'cache')
    os.mkdir(cache_directory)
    books_directory = os.path.join(working_directory, 'books')
    os.mkdir(books_directory)

def delete():
    working_directory = get_working_directory()
    try:
        shutil.rmtree(working_directory)
        print('WARNING: Deleting working directory...')
    except FileNotFoundError:
        pass
