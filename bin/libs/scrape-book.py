#!/usr/bin/python3

import argparse
import os
import sys
import tempfile

import db
import util
import workingdir


def main():

    parser = argparse.ArgumentParser(description='Create an HTML index from downloaded posts.')
    parser.add_argument('-blog', help='Blog ID (like from URL)', action='store', default=None, required=True)
    parser.add_argument('-ids', help='Comma separated list of post IDs', action='store', default=None, required=True)
    parser.add_argument('-title', help='Title for the book', action='store')
    parser.add_argument('-author', help='Author for the book', action='store')
    parser.add_argument('-comments', help='Use this flag to add comments to the back of the post book', action='store_true')
    args = parser.parse_args()

    ids = args.ids
    ids = ids.split(',')
    ids = [int(x.strip()) for x in ids]

    title = args.title
    author = args.author
    add_comments = args.comments

    books_directory = os.path.join(workingdir.get_working_directory(), 'books')
    cache_directory = os.path.join(workingdir.get_working_directory(), 'cache')

    metadatas = []

    for post_id in ids:
        metadata = util.get_full_cached_metadata_by_blog_id(args.blog, post_id)
        if metadata is None:
            print('Warning: Could not calculate metadata for post: {}.{}'.format(args.blog, post_id))
        else:
            metadatas.append(metadata)

    util.merge_mobi_metadata(metadatas, title=title, author=author, work_directory=cache_directory, output_directory=books_directory, add_comments=add_comments)


if __name__ == '__main__':
    sys.exit(main())
